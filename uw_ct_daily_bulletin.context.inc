<?php

/**
 * @file
 * uw_ct_daily_bulletin.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_daily_bulletin_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'daily_bulletin_preceding_archive';
  $context->description = 'Daily Bulletin Archive Preceding WCMS';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'daily-bulletin-archives' => 'daily-bulletin-archives',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_daily_bulletin-uw_db_preceding_archive_block' => array(
          'module' => 'uw_ct_daily_bulletin',
          'delta' => 'uw_db_preceding_archive_block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Daily Bulletin Archive Preceding WCMS');
  $export['daily_bulletin_preceding_archive'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'daily_bulletin_preceding_search';
  $context->description = 'Daily Bulletin Search Preceding WCMS';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'daily-bulletin-preceding-search' => 'daily-bulletin-preceding-search',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_daily_bulletin-uw_db_preceding_search_block' => array(
          'module' => 'uw_ct_daily_bulletin',
          'delta' => 'uw_db_preceding_search_block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Daily Bulletin Search Preceding WCMS');
  $export['daily_bulletin_preceding_search'] = $context;

  return $export;
}
