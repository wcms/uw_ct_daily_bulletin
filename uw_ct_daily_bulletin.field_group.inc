<?php

/**
 * @file
 * uw_ct_daily_bulletin.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_daily_bulletin_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_db_editor_information|node|uw_daily_bulletin|form';
  $field_group->group_name = 'group_db_editor_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_daily_bulletin';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Daily Bulletin Editor',
    'weight' => '9',
    'children' => array(
      0 => 'field_daily_bulletin_editor',
      1 => 'field_daily_bulletin_editor_depa',
      2 => 'field_daily_bulletin_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Daily Bulletin Editor',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-db-editor-information field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_db_editor_information|node|uw_daily_bulletin|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_mailchimp|node|uw_daily_bulletin|form';
  $field_group->group_name = 'group_mailchimp';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_daily_bulletin';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_uses';
  $field_group->data = array(
    'label' => 'MailChimp',
    'weight' => '8',
    'children' => array(
      0 => 'field_mailchimp_summary',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_mailchimp|node|uw_daily_bulletin|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sidebar|node|uw_daily_bulletin|form';
  $field_group->group_name = 'group_sidebar';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_daily_bulletin';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sidebar complementary content',
    'weight' => '8',
    'children' => array(
      0 => 'field_db_sidebar_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-sidebar field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_sidebar|node|uw_daily_bulletin|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload|node|uw_daily_bulletin|form';
  $field_group->group_name = 'group_upload';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_daily_bulletin';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload an image',
    'weight' => '7',
    'children' => array(
      0 => 'field_db_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-upload field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_upload|node|uw_daily_bulletin|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uses|node|uw_daily_bulletin|form';
  $field_group->group_name = 'group_uses';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_daily_bulletin';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Uses',
    'weight' => '6',
    'children' => array(
      0 => 'group_web',
      1 => 'group_mailchimp',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_uses|node|uw_daily_bulletin|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_web|node|uw_daily_bulletin|form';
  $field_group->group_name = 'group_web';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_daily_bulletin';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_uses';
  $field_group->data = array(
    'label' => 'Web',
    'weight' => '7',
    'children' => array(
      0 => 'body',
      1 => 'group_sidebar',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Web',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_web|node|uw_daily_bulletin|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Daily Bulletin Editor');
  t('MailChimp');
  t('Sidebar complementary content');
  t('Upload an image');
  t('Uses');
  t('Web');

  return $field_groups;
}
