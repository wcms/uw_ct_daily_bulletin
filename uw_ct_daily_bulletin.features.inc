<?php

/**
 * @file
 * uw_ct_daily_bulletin.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_daily_bulletin_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_daily_bulletin_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_daily_bulletin_node_info() {
  $items = array(
    'uw_daily_bulletin' => array(
      'name' => t('Daily Bulletin'),
      'base' => 'node_content',
      'description' => t('Daily Bulletins have main and sidebar content areas'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
