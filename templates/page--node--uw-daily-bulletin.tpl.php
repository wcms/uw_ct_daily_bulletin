<?php

/**
 * @file
 */

global $base_url;

global $language;
$langcode = $language->language;

// If creating a new node the language is not yet set so must set to undefined until node is saved.
if ($langcode == '' || $langcode == NULL) {
  $langcode = LANGUAGE_NONE;
}

// Get path arguments and last path.
$path_args = arg();
$last_path_arg = end($path_args);

// Check if we are looking at a draft or published node and load in resulting node.
if ($last_path_arg == "draft") {
  $node = workbench_moderation_node_current_load($node);
}
else {
  $node = node_load($path_args[1]);
}

// Parse the Title to get parts into link_date and then generate current date.
$current_date = drupal_get_path_alias();

// Generate current_link.
$current_link = drupal_get_path_alias();

// Load in content from node if there is any.
if (isset($node->body[$langcode][0]['value'])) {
  $content = $node->body[$langcode][0]['safe_value'];
  $anchor_tags = _uw_ct_daily_bulletin_get_anchor_tags($content);
}
else {
  $content = '';
}

$previous_link = _uw_ct_daily_bulletin_previous_next_link(drupal_get_path_alias(), 'previous');

$next_link = _uw_ct_daily_bulletin_previous_next_link(drupal_get_path_alias(), 'next');

$current_link = _uw_ct_daily_bulletin_current_link();

?>

<div class="breakpoint"></div>
<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site"<?php print $attributes; ?>>
  <div class="uw-site--inner">
    <div class="uw-section--inner">
      <div id="skip" class="skip">
        <a href="#main" class="element-invisible element-focusable uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
        <a href="#footer" class="element-invisible element-focusable  uw-site--element__invisible  uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
      </div>
    </div>

    <div id="header" class="uw-header--global">
      <?php
      $global_message = file_get_contents('https://uwaterloo.ca/global-message.html');
      if ($global_message) {
        ?>
        <div id="global-message">
          <?php echo $global_message; ?>
        </div>
        <?php
      }
      ?>
      <div class="uw-section--inner">
        <?php print render($page['global_header']); ?>
      </div>
    </div>

    <div id="site--offcanvas" class="uw-site--off-canvas">
      <div class="uw-section--inner">
        <?php print render($page['site_header']); ?>
      </div>
    </div>
 <?php if (variable_get('uw_theme_branding', 'full') == "full") { ?>
    <div id="site-colors" class="uw-site--colors">
            <div class="uw-section--inner">
                <div class="uw-site--cbar">
                    <div class="uw-site--c1 uw-cbar"></div>
                    <div class="uw-site--c2 uw-cbar"></div>
                    <div class="uw-site--c3 uw-cbar"></div>
                    <div class="uw-site--c4 uw-cbar"></div>
                </div>
            </div>
        </div>
 <?php } ?>
    <div id="site-header" class="uw-site--header">
      <div class="uw-section--inner">
        <a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home">
          <!--     <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"/>  -->
          <?php print $site_name; ?>
        </a>
      </div>
    </div>

    <div class="uw-header--banner__alt">
      <div class="uw-section--inner">
        <?php print render($page['banner_alt']); ?>
      </div><!-- /uw-headeranner__alt -->
    </div>

    <div id="main" class="uw-site--main">
      <div class="uw-section--inner">
        <div id="site-navigation-wrapper" class="uw-site-navigation--wrapper">
          <div id="site-specific-nav" class="uw-site-navigation--specific">
            <div id="site-navigation" class="uw-site-navigation">
              <?php if (user_access('access content')) {print render($page['sidebar_first']);
              } ?>
            </div>
          </div>
        </div>
      <div class="uw-site-main--content">
        <div class="uw-site--main-top">
          <div class="uw-site--banner">
            <?php print render($page['banner']); ?>
          </div>

          <div class="uw-site--messages">
            <?php print $messages; ?>
          </div>

          <div class="uw-site--help">
            <?php print render($page['help']); ?>
          </div>

          <div class="uw-site--breadcrumb">
            <?php print $breadcrumb; ?>
          </div>

          <div class="uw-site--title">
            <?php list($title, $content) = uw_fdsu_theme_separate_h1_and_content($page, $title); ?>
            <h1><?php print $title ? $title : $site_name; ?></h1>
          </div>
        </div>

        <div class="db_header_box_full_width_clear"></div>

        <div class="db_full_width">
          <div class="db_header_box_1">
            <?php
              $previous_next = '';
              if ($previous_link !== "" && $next_link !== "") {
                if ($current_link) {
                  if ($next_link == $current_link) {
                    $previous_next = $previous_next .= '<a href="' . $base_url . '/' . $previous_link . '">Previous</a> | <a href="' . $base_url . '">Current</a>';
                  }
                  else {
                    $previous_next .= '<a href="' . $base_url . '/' . $previous_link . '">Previous</a> | <a href="' . $base_url . '/' . $next_link . '">Next</a> | <a href="' . $base_url . '">Current</a>';
                  }
                }
                else {
                  $previous_next .= '<a href="' . $base_url . '/' . $previous_link . '">Previous</a> | <a href="' . $base_url . '/' . $next_link . '">Next</a>';
                }
              }
              elseif ($next_link !== "") {
                $previous_next .= '<a href="daily-bulletin-archive">Archives</a> | <a href="' . $base_url . '/' . $next_link . '">Next</a>';
              }
              elseif ($previous_link !== "") {
                $previous_next .= '<a href="' . $base_url . '/' . $previous_link . '">Previous</a>';
              }
              elseif ($previous_link == "") {
                $previous_next .= '<a href="daily-bulletin-archive">Archives</a>';
              }
              else {
                $previous_next .= '<br />';
              }

              $previous_next .= '<br />';

              print $previous_next;
            ?>
          </div>

          <div class="db_header_box_2">
            <div class="rss_link">
              <a href="<?php print $base_url; ?>/index.xml">Daily Bulletin feed</a>
            </div>
          </div>

          <div class="db_header_box_3">
            <?php
              $html = '';
              $html .= '<ul>';

              if (isset($anchor_tags)) {
                foreach($anchor_tags as $anchor_tag) {
                  $html .= '<li><a href="#' . $anchor_tag['id'] . '">' . $anchor_tag['title'] . '</a></li>';
                }
              }
              else {
                $html .= '&nbsp;';
              }

              $html .= '</ul>';
              print $html;
            ?>
          </div>

          <div class="db_header_box_4">
            <p>
              Editor:<br />
              <?php
                if (isset($node->field_daily_bulletin_editor[$langcode][0]['value'])) {
                  print $node->field_daily_bulletin_editor[$langcode][0]['value'];
                }
              ?>
              <br />
              <?php
                if (isset($node->field_daily_bulletin_editor_depa[$langcode][0]['value'])) {
                  print $node->field_daily_bulletin_editor_depa[$langcode][0]['value'];
                }
              ?>
              <br />
              <?php
                if (isset($node->field_daily_bulletin_email[$langcode][0]['email'])) {
                  print '<a href="mailto:' . $node->field_daily_bulletin_email[$langcode][0]['email'] . '">' . $node->field_daily_bulletin_email[$langcode][0]['email'] . '</a>';
                }
              ?>
            </p>
          </div>
        </div>

        <div class="db_header_box_full_width_clear"></div>
          <!-- when logged in -->
          <?php if ($tabs): ?>
            <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?></div><?php
          endif; ?>
              <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul>
              <?php endif; ?>

                <div id="content" class="uw-site-content">
                  <br />
                  <?php
                    list($title, $displayed_content) = uw_fdsu_theme_separate_h1_and_content($page, $title);

                    if (isset($anchor_tags)) {
                      if (count($anchor_tags) > 0) {
                        foreach($anchor_tags as $anchor_tag) {
                          $h2_text = str_replace('<h2>', '', $anchor_tag['full_tag']);
                          $h2_text = str_replace('</h2>', '', $h2_text);
                          $replaced_html = '<h2 id="' . $anchor_tag['id'] . '">' . html_entity_decode($h2_text) . '</h2>';
                          $anchor_tag['full_tag'] = '<h2>' . htmlentities(html_entity_decode($h2_text, ENT_QUOTES), ENT_NOQUOTES) . '</h2>';
                          $displayed_content = str_replace($anchor_tag['full_tag'], $replaced_html, $displayed_content);
                        }
                      }
                    }
                    print $displayed_content;
                  ?>
                  <div class="uw-daily-bulletin-bottom-previous-next">
                    <?php print $previous_next; ?>
                  </div>
                </div><!--/main-content-->

                <?php $sidebar = render($node->field_db_sidebar_content[$langcode][0]['value']); ?>
                <?php $sidebar_promo = render($page['promo']); ?>
                <?php if (isset($sidebar) || isset($sidebar_promo)) { ?>
                  <?php if (($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '')) { ?>
                    <div id="site-sidebar-wrapper" class="uw-site-sidebar--wrapper">
                      <div id="site-sidebar-nav" class="uw-site-sidebar--nav">
                        <div id="site-sidebar" class="uw-site-sidebar <?php if ($sidebar_promo !== NULL && $sidebar_promo !== '') {echo ('sticky-promo');
                       } ?>">

                          <?php if (isset($sidebar_promo)) { ?>
                          <?php if ($sidebar_promo !== NULL && $sidebar_promo !== '') { ?>
                          <div class="uw-site-sidebar--promo">
                            <?php print render($page['promo']); ?>
                          </div>
                          <?php } ?>
                          <?php } ?>

                      <div class="uw-site-sidebar--second">
                        <?php print $sidebar; ?>
                      </div>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                <?php } ?>
          </div>
        </div><!--/section inner-->
    </div><!--/site main-->

    <div class="uw-site--watermark">
        <div class="uw-section--inner"></div>
    </div>

    <div id="site-footer" class="uw-site-footer site-footer-toggle open-site-footer">
        <div class="uw-section--inner">
          <?php if (empty($page['site_footer'])): ?>
              <div class="uw-site-footer1 uw-no-site-footer">
          <?php else : ?>
              <div class="uw-site-footer1">
          <?php endif; ?>

          <div class="uw-site-footer1--logo-dept">
            <?php
              $site_logo = variable_get('uw_nav_site_footer_logo');
              $site_logo_link = variable_get('uw_nav_site_footer_logo_link');
              $facebook = variable_get('uw_nav_site_footer_facebook');
              $twitter = variable_get('uw_nav_site_footer_twitter');
              $instagram = variable_get('uw_nav_site_footer_instagram');
              $youtube = variable_get('uw_nav_site_footer_youtube');
              $linkedin = variable_get('uw_nav_site_footer_linkedin');
              ?>

              <?php if (isset($site_logo_link)) : ?>
                <a href="<?php print $site_logo_link; ?>"><img src="<?php print base_path() . drupal_get_path('module', 'uw_nav_site_footer'); ?>/logos/<?php print variable_get('uw_nav_site_footer_logo'); ?>.png" /></a>
              <?php else : ?>
                <?php  $site_name = (strtolower($site_name)); print '<a href="' . url('<front>') . '">' . (ucfirst($site_name)) . '</a>';  ?>
              <?php endif; ?>

            </div>

            <div class="uw-site-footer1--contact">
              <ul class="uw-footer-social">
                <?php if ($facebook !== "" && $facebook !== NULL): ?>
                  <li>
                    <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>">
                      <i class="ifdsu fdsu-facebook"></i>
                      <span class="off-screen">facebook</span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ($twitter !== "" && $twitter !== NULL): ?>
                  <li>
                    <a href="https://www.twitter.com/<?php print check_plain($twitter); ?>">
                      <i class="ifdsu fdsu-twitter"></i>
                      <span class="off-screen">twitter</span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ($youtube !== "" && $youtube !== NULL): ?>
                  <li>
                    <a href="<?php print check_plain($youtube); ?>">
                      <i class="ifdsu fdsu-youtube"></i>
                      <span class="off-screen">youtube</span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ($instagram !== "" && $instagram !== NULL): ?>
                  <li>
                    <a href="<?php print check_plain($instagram); ?>">
                      <i class="ifdsu fdsu-instagram"></i>
                      <span class="off-screen">instagram</span>
                    </a>
                  </li>
                <?php endif; ?>

                <?php if ($linkedin !== "" && $linkedin !== NULL): ?>
                  <li>
                    <a href="<?php check_plain(print $linkedin); ?>">
                      <i class="ifdsu fdsu-linkedin"></i>
                      <span class="off-screen">linkedin</span>
                    </a>
                  </li>
                <?php endif; ?>

              </ul>

            </div>
            <?php if (!empty($page['site_footer'])): ?>
              <div class="uw-site-footer2">
                <?php print render($page['site_footer']); ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <?php print render($page['global_footer']); ?>
    </div>
  </div>
</div><!--/site-->
<div class="ie-resize-fix"></div>
