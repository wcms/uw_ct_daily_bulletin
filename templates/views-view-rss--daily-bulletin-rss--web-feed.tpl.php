<?php

/**
 * @file
 * Default template for feed displays that use the RSS style.
 *
 * @ingroup views_templates
 */
  global $language;
  $langcode = $language->language;

  // Need to remove the guid, pudDate and creator tags in order for
  // tags required by Daily Bulletin RSS and block on pathway pages.
  $items = preg_replace('/<guid(.*)<\/guid>/', '', $items);
  $items = preg_replace('/<pubDate>(.*)<\/pubDate>/', '', $items);
  $items = preg_replace('/<dc\:creator>(.*)<\/dc\:creator>/', '', $items);

  // Get most current published Daily Bulletin, required for title
  // and sidebar content.
  $bulletin = db_select('field_data_field_bulletin_date', 'bd');
  $bulletin->innerJoin('node_revision', 'nr', 'nr.nid = bd.entity_id');
  $bulletin->fields('bd', array('field_bulletin_date_value'));
  $bulletin->fields('nr', array('nid'));
  $bulletin->condition('nr.status', 1, '=');
  $bulletin->orderBy('field_bulletin_date_value', 'DESC');
  $bulletin->range(0, 1);

  $bulletin = $bulletin->execute()->fetchAssoc();

  // Get current published node.
  $bulletin_node = node_load($bulletin['nid']);

  // Get currenly published content and anchor tags.
  $bulletin_content = $bulletin_node->body[$langcode][0]['value'];
  $anchor_tags = _uw_ct_daily_bulletin_get_anchor_tags($bulletin_content);

  // Need to insert author, Daily Bulletin guid tag and anchor tags after description tag.
  // This is required from old Daily Bulletin RSS format and the Daily Bulletin block
  // on the pathway pages as well as the portal.
  $str_to_insert = '';
  $pos = strrpos($items, '</link>') + 7;
  $str_to_insert = '<guid>https://uwaterloo.ca/daily-bulletin/' . str_replace(' 00:00:00', '', $bulletin_node->field_bulletin_date[$langcode][0]['value']) . '</guid>';
  $str_to_insert .= '<author>' . $bulletin_node->field_daily_bulletin_editor[$langcode][0]['value'] . '</author>';
  $items = substr($items, 0, $pos) . $str_to_insert . substr($items, $pos);

  $str_to_insert = '';
  $pos = strrpos($items, '<description>') + 13;
  $str_to_insert .= '<![CDATA[';
  $str_to_insert .= '<h2>' . $bulletin_node->title . '</h2>';
  $str_to_insert .= '<div class="headline_summary">';
  $str_to_insert .= '<ul>';
  foreach($anchor_tags as $anchor_tag) {
    $str_to_insert .= '<li><strong>' . $anchor_tag['title'] . '</strong></li>';
  }
  $str_to_insert .= '</ul>';
  $str_to_insert .= '</div>';
  $str_to_insert .= ']]>';
  $items = substr($items, 0, $pos) . $str_to_insert . substr($items, $pos);

  $str_to_insert = '';
  // Need to add in the sidebar content after the description tag.
  $pos = strrpos($items, '</description>');
  $str_to_insert = '<![CDATA[ <hr />' . $bulletin_node->field_db_sidebar_content[$langcode][0]['value'] . ']]>';
  $items = substr($items, 0, $pos) . $str_to_insert . substr($items, $pos);

?>
<?php print "<?xml"; ?> version="1.0" encoding="utf-8" <?php print "?>"; ?>
<rss version="2.0" xml:base="<?php print $link; ?>"<?php print $namespaces; ?>>
  <channel>
    <title>University of Waterloo - The Daily Bulletin</title>
    <link><?php global $base_url; print $base_url; ?></link>
    <description><?php print $description; ?></description>
    <language><?php print $langcode; ?></language>
        <?php print $channel_elements; ?>
    <?php print $items; ?>
  </channel>
</rss>
